//
//  CartItem.swift
//  Garcom
//
//  Created by Gilson Gil on 12/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct CartItem {
  let product: Product
  let amount: Int
  var total: Float {
    return product.value * Float(amount)
  }
  var formattedTotal: String {
    return String(format: "R$%.2f", arguments: [total])
  }
}
