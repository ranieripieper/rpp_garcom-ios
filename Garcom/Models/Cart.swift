//
//  Cart.swift
//  Garcom
//
//  Created by Gilson Gil on 12/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

final class Cart {
  var items = [CartItem]()
  var total: Float {
    let total = items.reduce(0) { sum, item -> Float in
      return sum + item.total
    }
    return total
  }
  var formattedTotal: String {
    return String(format: "R$%.2f", arguments: [total])
  }
  var formattedTotalWithService: String {
    return String(format: "R$%.2f", arguments: [total + 1.5])
  }
  
  static let shared = Cart()
  
  func add(item: CartItem) {
    self.items.append(item)
  }
  
  func pay() {
    History.shared.payed(items: items)
    self.items.removeAll()
  }
}
