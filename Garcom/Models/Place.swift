//
//  Place.swift
//  Garcom
//
//  Created by Gilson Gil on 08/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct Place {
  let name: String
  let address: String
  let distance: String
  let imageName: String
  
  static func mockPlaces() -> [Place] {
    let place1 = Place(name: "Casa do Pão de Queijo", address: "Av. Paulista", distance: "2.3km", imageName: "img_list_1")
    let place2 = Place(name: "Casa do Pão de Queijo", address: "Av. Brig. Luis Antônio", distance: "1.8km", imageName: "img_list_2")
    let place3 = Place(name: "Casa do Pão de Queijo", address: "Rua Tutóia", distance: "1.0km", imageName: "img_list_3")
    let place4 = Place(name: "Casa do Pão de Queijo", address: "Rua Anchieta", distance: "4.0km", imageName: "img_list_4")
    let place5 = Place(name: "Casa do Pão de Queijo", address: "Rua Aurora", distance: "4.4km", imageName: "img_list_5")
    
    return [place1, place2, place3, place4, place5]
  }
}
