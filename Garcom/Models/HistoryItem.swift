//
//  HistoryItem.swift
//  Garcom
//
//  Created by Gilson Gil on 12/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

enum State {
  case delivered, refused
}

struct HistoryItem {
  let items: [CartItem]
  let date: Date
  let state: State
  
  var total: Float {
    return items.reduce(0) { sum, item -> Float in
      sum + item.total
    }
  }
  var formattedTotal: String {
    return String(format: "R$%.2f", arguments: [total + 1.5])
  }
  
  init(items: [CartItem]) {
    self.items = items
    self.date = Date()
    self.state = (arc4random() % 2 == 0) ? .delivered : .refused
  }
}
