//
//  History.swift
//  Garcom
//
//  Created by Gilson Gil on 12/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

final class History {
  var items = [HistoryItem]()
  var groupedItems: [[HistoryItem]] {
    let dates = items.flatMap { Calendar.current.component(Calendar.Component.day, from: $0.date) }
    let datesSet = Set(dates).sorted { lhs, rhs -> Bool in
      rhs < lhs
    }
    let grouped = datesSet.flatMap { date -> [HistoryItem]? in
      items.filter {
        Calendar.current.component(Calendar.Component.day, from: $0.date) == date
      }
    }
    return grouped
  }
  
  static let shared = History()
  
  func payed(items: [CartItem]) {
    let historyItem = HistoryItem(items: items)
    self.items.append(historyItem)
  }
}
