//
//  Product.swift
//  Garcom
//
//  Created by Gilson Gil on 09/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct Product {
  let name: String
  let value: Float
  let description: String
  var formattedValue: String {
    return String(format: "R$%.2f", arguments: [value])
  }
  
  static func mockFoodProducts() -> [Product] {
    let product1 = Product(name: "Queijo Quente Mineirinho Grill", value: 7.50, description: "Pão de centeio com queijo branco e peito de perú.")
    let product2 = Product(name: "Club Light | Club Light Centeio", value: 9.50, description: "Pão de centeio com queijo branco e peito de perú.")
    let product3 = Product(name: "Misto Presunto e Queijo Grill", value: 10.00, description: "Pão de centeio com queijo branco e peito de perú.")
    let product4 = Product(name: "Filé de Frango Grill", value: 8.50, description: "Pão de centeio com queijo branco e peito de perú.")
    let product5 = Product(name: "Beirute de Frios Grill", value: 9.50, description: "Pão de centeio com queijo branco e peito de perú.")
    let product6 = Product(name: "Beirute de Peru Grill", value: 10.50, description: "Pão de centeio com queijo branco e peito de perú.")
    let product7 = Product(name: "Beirute de Rosbife Grill", value: 11.00, description: "Pão de centeio com queijo branco e peito de perú.")
    let product8 = Product(name: "Italiano Ciabatta Grill", value: 9.00, description: "Pão de centeio com queijo branco e peito de perú.")
    
    return [product1, product2, product3, product4, product5, product6, product7, product8]
  }
  
  static func mockDrinkProducts() -> [Product] {
    let product1 = Product(name: "Água Mineral sem Gás", value: 3.50, description: "")
    let product2 = Product(name: "Água Mineral com Gás", value: 3.50, description: "")
    let product3 = Product(name: "Suco de Laranja Natural", value: 9.00, description: "")
    let product4 = Product(name: "Suco de Açaí", value: 10.50, description: "")
    let product5 = Product(name: "Café Espresso", value: 2.50, description: "")
    let product6 = Product(name: "Chá Gelado", value: 6.50, description: "")
    let product7 = Product(name: "Chá Quente", value: 8.00, description: "")
    let product8 = Product(name: "Refrigerante (lata)", value: 6.00, description: "")
    
    return [product1, product2, product3, product4, product5, product6, product7, product8]
  }
}
