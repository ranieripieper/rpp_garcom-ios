//
//  Category.swift
//  Garcom
//
//  Created by Gilson Gil on 08/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct Category {
  let name: String
  let imageName: String
  
  static func mockCategories() -> [Category] {
    let category1 = Category(name: "Sanduiches", imageName: "img_menu_sanduiches")
    let category2 = Category(name: "Paninis", imageName: "img_menu_paninis")
    let category3 = Category(name: "Bebidas Frias", imageName: "img_menu_bebidas_frias")
    let category4 = Category(name: "Folhado Salgado", imageName: "img_menu_folhados")
    let category5 = Category(name: "Pão de Queijo", imageName: "img_menu_pao_de_queijo")
    let category6 = Category(name: "Bebidas Quentes", imageName: "img_menu_bebidas_quentes")
    let category7 = Category(name: "Pastel de Forno da Vovó", imageName: "img_menu_pastel")
    let category8 = Category(name: "Exclusividades da Casa", imageName: "img_menu_exclusividade")
    let category9 = Category(name: "Doces da casa", imageName: "img_menu_doces")
    
    return [category1, category2, category3, category4, category5, category6, category7, category8, category9]
  }
}
