//
//  SignupViewController.swift
//  Garcom
//
//  Created by Gilson Gil on 26/10/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class SignupViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    let keyboardHandler = KeyboardHandlerViewController(scrollView: scrollView)
    keyboardHandler.willMove(toParentViewController: self)
    keyboardHandler.view.frame = .zero
    view.addSubview(keyboardHandler.view)
    addChildViewController(keyboardHandler)
    keyboardHandler.didMove(toParentViewController: self)
  }
}

// MARK: - Actions
extension SignupViewController {
  @IBAction func signupTapped() {
    UIApplication.shared.keyWindow?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
  }
  
  @IBAction func tapped() {
    view.endEditing(true)
  }
}

// MARK: - UITextField Delegate
extension SignupViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    let nextTag = textField.tag + 1
    guard let nextTextField = view.viewWithTag(nextTag) as? TextField else {
      textField.resignFirstResponder()
      return true
    }
    nextTextField.becomeFirstResponder()
    return true
  }
  
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    if let pickerView = textField.inputView as? UIPickerView {
      pickerView.dataSource = self
      pickerView.delegate = self
    }
    let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: 44))
    let spacing = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    let ok = UIBarButtonItem(title: "OK", style: .plain, target: self, action: #selector(tapped))
    toolbar.items = [spacing, ok]
    textField.inputAccessoryView = toolbar
    return true
  }
  
  func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    if let datePicker = textField.inputView as? UIDatePicker {
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "dd/MM/yyyy"
      textField.text = dateFormatter.string(from: datePicker.date)
    } else if let pickerView = textField.inputView as? UIPickerView {
      textField.text = pickerView.delegate?.pickerView?(pickerView, titleForRow: pickerView.selectedRow(inComponent: 0), forComponent: 0)
    }
    return true
  }
}

extension SignupViewController: UIPickerViewDataSource {
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return 3
  }
}

extension SignupViewController: UIPickerViewDelegate {
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    if row == 0 {
      return "Feminino"
    } else if row == 1 {
      return "Masculino"
    } else {
      return "Prefiro não declarar"
    }
  }
}
