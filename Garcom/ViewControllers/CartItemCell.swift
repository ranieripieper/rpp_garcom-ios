//
//  CartItemCell.swift
//  Garcom
//
//  Created by Gilson Gil on 12/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class CartItemCell: UITableViewCell {
  @IBOutlet weak var amountLabel: UILabel!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var valueLabel: UILabel!
  
  func configure(with item: CartItem) {
    amountLabel.text = String(item.amount)
    nameLabel.text = item.product.name
    valueLabel.text = item.formattedTotal
  }
}
