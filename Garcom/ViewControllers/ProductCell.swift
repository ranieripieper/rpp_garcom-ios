//
//  ProductCell.swift
//  Garcom
//
//  Created by Gilson Gil on 09/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class ProductCell: UITableViewCell {
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var valueLabel: UILabel!
  
  func configure(with product: Product) {
    nameLabel.text = product.name
    valueLabel.text = product.formattedValue
  }
}
