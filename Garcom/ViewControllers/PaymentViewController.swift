//
//  PaymentViewController.swift
//  Garcom
//
//  Created by Gilson Gil on 12/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class PaymentViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var valueLabel: UILabel!
  @IBOutlet weak var totalLabel: UILabel!
  
  let items = Cart.shared.items
  
  override func awakeFromNib() {
    super.awakeFromNib()
    automaticallyAdjustsScrollViewInsets = false
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    let keyboardHandler = KeyboardHandlerViewController(scrollView: nil)
    keyboardHandler.willMove(toParentViewController: self)
    keyboardHandler.view.frame = .zero
    view.addSubview(keyboardHandler.view)
    addChildViewController(keyboardHandler)
    keyboardHandler.didMove(toParentViewController: self)
    keyboardHandler.delegate = self
    valueLabel.text = Cart.shared.formattedTotal
    totalLabel.text = Cart.shared.formattedTotalWithService
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesBegan(touches, with: event)
    view.endEditing(true)
  }
}

extension PaymentViewController {
  @IBAction func pay(sender: UIButton) {
    Cart.shared.pay()
    let _ = navigationController?.popToRootViewController(animated: true)
  }
}

// MARK: - UITableView DataSource
extension PaymentViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let item = items[indexPath.row]
    guard let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(PaymentCell.self).components(separatedBy: ".").last!, for: indexPath) as? PaymentCell else {
      return UITableViewCell()
    }
    cell.configure(with: item)
    return cell
  }
}

extension PaymentViewController: KeyboardHandlerViewControllerDelegate {
  func keyboardDidDisapper() {
    UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
      self.view.transform = CGAffineTransform.identity
    }, completion: nil)
  }
  
  func keyboardDidAppear(height: CGFloat) {
    UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
      self.view.transform = CGAffineTransform.identity.translatedBy(x: 0, y: -height)
    }, completion: nil)
  }
}
