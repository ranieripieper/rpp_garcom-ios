//
//  CartViewController.swift
//  Garcom
//
//  Created by Gilson Gil on 12/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol CartViewControllerDelegate: class {
  func purchaseTapped()
}

final class CartViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  fileprivate var cartSummaryFooter: CartSummaryFooter?
  
  weak var delegate: CartViewControllerDelegate?
  
  let items = Cart.shared.items
  
  override func viewDidLoad() {
    super.viewDidLoad()
    cartSummaryFooter = Bundle.main.loadNibNamed("CartSummaryFooter", owner: self, options: nil)?.first as? CartSummaryFooter
  }
}

// MARK: - Actions
extension CartViewController {
  @IBAction func purchase(sender: UIButton) {
    
  }
}

// MARK: - UITableView DataSource
extension CartViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let item = items[indexPath.row]
    guard let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(CartItemCell.self).components(separatedBy: ".").last!, for: indexPath) as? CartItemCell else {
      return UITableViewCell()
    }
    cell.configure(with: item)
    return cell
  }
}

// MARK: - UITableView Delegate
extension CartViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    cartSummaryFooter?.delegate = self
    cartSummaryFooter?.backgroundColor = UIColor(white: 243 / 255, alpha: 1)
    return cartSummaryFooter
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 233
  }
}

// MARK: - CartSummaryDelegate
extension CartViewController: CartSummaryFooterDelegate {
  func purchaseTapped() {
    delegate?.purchaseTapped()
  }
}
