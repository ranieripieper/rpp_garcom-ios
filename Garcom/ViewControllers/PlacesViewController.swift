//
//  PlacesViewController.swift
//  Garcom
//
//  Created by Gilson Gil on 08/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class PlacesViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  
  fileprivate let places = Place.mockPlaces()
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
//    navigationController?.isNavigationBarHidden = false
    guard let indexPath = tableView.indexPathForSelectedRow else {
      return
    }
    tableView.deselectRow(at: indexPath, animated: true)
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
//    navigationController?.isNavigationBarHidden = true
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
  }
}

// MARK: - UITableView DataSource
extension PlacesViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return places.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let place = places[indexPath.row]
    guard let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(PlaceCell.self).components(separatedBy: ".").last!, for: indexPath) as? PlaceCell else {
      return UITableViewCell()
    }
    cell.configure(with: place)
    return cell
  }
}

// MARK: - UITableView Delegate
extension PlacesViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
}
