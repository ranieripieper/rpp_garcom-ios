//
//  SettingsViewController.swift
//  Garcom
//
//  Created by Gilson Gil on 12/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class SettingsViewController: UIViewController {
  
}

extension SettingsViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 2
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if indexPath.row == 0 {
      let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsLogoutCell")!
      return cell
    } else {
      let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsDeleteAccountCell")!
      return cell
    }
  }
}

extension SettingsViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    Cart.shared.items.removeAll()
    if indexPath.row == 1 {
      History.shared.items.removeAll()
    }
    UIApplication.shared.keyWindow?.rootViewController = UIStoryboard(name: "Login", bundle: nil).instantiateInitialViewController()
  }
}
