//
//  LoginViewController.swift
//  Garcom
//
//  Created by Gilson Gil on 04/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class LoginViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var backButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    let keyboardHandler = KeyboardHandlerViewController(scrollView: scrollView)
    keyboardHandler.willMove(toParentViewController: self)
    keyboardHandler.view.frame = .zero
    view.addSubview(keyboardHandler.view)
    addChildViewController(keyboardHandler)
    keyboardHandler.didMove(toParentViewController: self)
    backButton.transform = CGAffineTransform.identity.rotated(by: CGFloat(M_PI))
  }
}

// MARK: - Actions
extension LoginViewController {
  @IBAction func loginTapped(sender: UIButton) {
    UIApplication.shared.keyWindow?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
  }
  
  @IBAction func tapped() {
    view.endEditing(true)
  }
  
  @IBAction func backTapped() {
    let _ = navigationController?.popViewController(animated: true)
  }
}

// MARK: - UITextField Delegate
extension LoginViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    let nextTag = textField.tag + 1
    guard let nextTextField = view.viewWithTag(nextTag) as? TextField else {
      textField.resignFirstResponder()
      return true
    }
    nextTextField.becomeFirstResponder()
    return true
  }
  
  func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    if let datePicker = textField.inputView as? UIDatePicker {
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "dd/MM/yyyy"
      textField.text = dateFormatter.string(from: datePicker.date)
    } else if let pickerView = textField.inputView as? UIPickerView {
      textField.text = pickerView.delegate?.pickerView?(pickerView, titleForRow: pickerView.selectedRow(inComponent: 0), forComponent: 0)
    }
    return true
  }
}
