//
//  CategoriesViewController.swift
//  Garcom
//
//  Created by Gilson Gil on 08/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class CategoriesViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  
  var tabbarController: TabBarController?
  fileprivate let categories = Category.mockCategories()
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    guard let indexPath = tableView.indexPathForSelectedRow else {
      return
    }
    tableView.deselectRow(at: indexPath, animated: true)
  }
}

// MARK: - Actions
extension CategoriesViewController {
  func selected(category: Category) {
    tabbarController?.performSegue(withIdentifier: "SegueProduct", sender: category)
  }
}

// MARK: - UITableView DataSource
extension CategoriesViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return categories.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let category = categories[indexPath.row]
    guard let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(CategoryCell.self).components(separatedBy: ".").last!, for: indexPath) as? CategoryCell else {
      return UITableViewCell()
    }
    cell.configure(with: category)
    return cell
  }
}

// MARK: - UITableView Delegate
extension CategoriesViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let category = categories[indexPath.row]
    selected(category: category)
  }
}
