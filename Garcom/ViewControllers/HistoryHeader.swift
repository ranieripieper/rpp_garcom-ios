//
//  HistoryHeader.swift
//  Garcom
//
//  Created by Gilson Gil on 12/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class HistoryHeader: UITableViewHeaderFooterView {
  @IBOutlet weak var label: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    contentView.backgroundColor = UIColor(red: 84 / 255, green: 105 / 255, blue: 121 / 255, alpha: 1)
  }
  
  func configure(with date: String) {
    label.text = date
  }
}
