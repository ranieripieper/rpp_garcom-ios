//
//  HistoryViewController.swift
//  Garcom
//
//  Created by Gilson Gil on 12/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class HistoryViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  
  fileprivate let items = History.shared.groupedItems
  fileprivate let dateFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.dateStyle = .short
    dateFormatter.doesRelativeDateFormatting = true
    return dateFormatter
  }()
  fileprivate let timeFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.timeStyle = .short
    return dateFormatter
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.register(UINib(nibName: "HistoryHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: NSStringFromClass(HistoryHeader.self).components(separatedBy: ".").last!)
    tableView.estimatedRowHeight = 150
  }
}

// MARK: - UITableView DataSource
extension HistoryViewController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return items.count
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items[section].count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let item = items[indexPath.section][indexPath.row]
    guard let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(HistoryCell.self).components(separatedBy: ".").last!, for: indexPath) as? HistoryCell else {
      return UITableViewCell()
    }
    cell.configure(with: item, timeFormatter: timeFormatter)
    return cell
  }
}

// MARK: - UITableView Delegate
extension HistoryViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    guard let date = items[section].first?.date else {
      return nil
    }
    let text = dateFormatter.string(from: date)
    let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: NSStringFromClass(HistoryHeader.self).components(separatedBy: ".").last!) as! HistoryHeader
    header.configure(with: text)
    return header
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 40
  }
}
