//
//  ProductsViewController.swift
//  Garcom
//
//  Created by Gilson Gil on 09/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class ProductsViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var cartItem: UIButton!
  
  var products: [Product]?
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    checkCart()
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    if let viewController = segue.destination as? PurchaseViewController {
      guard let cell = sender as? UITableViewCell, let indexPath = tableView.indexPath(for: cell) else {
        return
      }
      guard let product = products?[indexPath.row] else {
        return
      }
      viewController.product = product
    } else if let cartViewController = segue.destination as? CartViewController {
      cartViewController.delegate = self
    }
  }
  
  override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
    guard identifier == "SegueCart" else {
      return true
    }
    return Cart.shared.items.count > 0
  }
}

// MARK: - Actions
extension ProductsViewController {
  func selected(product: Product) {
    
  }
}

// MARK: - UITableView DataSource
extension ProductsViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return products?.count ?? 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let product = products?[indexPath.row] else {
      return UITableViewCell()
    }
    guard let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(ProductCell.self).components(separatedBy: ".").last!, for: indexPath) as? ProductCell else {
      return UITableViewCell()
    }
    cell.configure(with: product)
    return cell
  }
}

// MARK: - Public
extension ProductsViewController {
  func checkCart() {
    if Cart.shared.items.count > 0 {
      cartItem.setImage(UIImage(named: "iconNewItem"), for: .normal)
    } else {
      cartItem.setImage(UIImage(named: "icnCart"), for: .normal)
    }
    guard let indexPath = tableView.indexPathForSelectedRow else {
      return
    }
    tableView.deselectRow(at: indexPath, animated: true)
  }
}

// MARK: - UITableView Delegate
extension ProductsViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
}

extension ProductsViewController: CartViewControllerDelegate {
  func purchaseTapped() {
    dismiss(animated: true) {
      self.performSegue(withIdentifier: "SeguePaymentViewController", sender: nil)
    }
  }
}
