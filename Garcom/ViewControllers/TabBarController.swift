//
//  TabBarController.swift
//  Garcom
//
//  Created by Gilson Gil on 09/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class TabBarController: UIViewController {
  @IBOutlet weak var categoriesButton: UIButton!
  @IBOutlet weak var historyButton: UIButton!
  @IBOutlet weak var settingsButton: UIButton!
  @IBOutlet weak var tabBar: UIView!
  @IBOutlet weak var cartItem: UIButton!
  
  fileprivate var currentViewController: UIViewController?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    presentCategories()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    checkCart()
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    if let productsViewController = segue.destination as? ProductsViewController {
      guard let category = sender as? Category else {
        return
      }
      if category.name == "Pastel de Forno da Vovó" || category.name == "Pão de Queijo" || category.name == "Folhado Salgado" || category.name == "Paninis" || category.name == "Sanduiches" {
        productsViewController.products = Product.mockFoodProducts()
      } else if category.name == "Bebidas Quentes" || category.name == "Bebidas Frias" {
        productsViewController.products = Product.mockDrinkProducts()
      }
    }
  }
  
  override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
    if identifier == "SegueCart" {
      return Cart.shared.items.count > 0
    } else {
      guard let category = sender as? Category else {
        return false
      }
      return category.name == "Pastel de Forno da Vovó" || category.name == "Pão de Queijo" || category.name == "Folhado Salgado" || category.name == "Paninis" || category.name == "Sanduiches" || category.name == "Bebidas Quentes" || category.name == "Bebidas Frias"
    }
  }
}

// MARK: - Actions
extension TabBarController {
  @IBAction func categoriesTapped() {
    presentCategories()
  }
  
  @IBAction func historyTapped() {
    presentHistory()
  }
  
  @IBAction func settingsTapped() {
    presentSettings()
  }
}

// MARK: - Private
fileprivate extension TabBarController {
  func presentCategories() {
    guard !(currentViewController is CategoriesViewController) else {
      return
    }
    categoriesButton.isSelected = true
    historyButton.isSelected = false
    settingsButton.isSelected = false
    guard let categoriesViewController = storyboard?.instantiateViewController(withIdentifier: "CategoriesViewController") as? CategoriesViewController else {
      return
    }
    categoriesViewController.tabbarController = self
    presentViewController(categoriesViewController)
  }
  
  func presentHistory() {
    guard !(currentViewController is HistoryViewController) else {
      return
    }
    categoriesButton.isSelected = false
    historyButton.isSelected = true
    settingsButton.isSelected = false
    guard let historyViewController = storyboard?.instantiateViewController(withIdentifier: "HistoryViewController") as? HistoryViewController else {
      return
    }
    presentViewController(historyViewController)
  }
  
  func presentSettings() {
    guard !(currentViewController is SettingsViewController) else {
      return
    }
    categoriesButton.isSelected = false
    historyButton.isSelected = false
    settingsButton.isSelected = true
    guard let settingsViewController = storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else {
      return
    }
    presentViewController(settingsViewController)
  }
  
  func presentViewController(_ viewController: UIViewController) {
    currentViewController = viewController
    viewController.willMove(toParentViewController: self)
    view.insertSubview(viewController.view, belowSubview: tabBar)
    addChildViewController(viewController)
    viewController.view.frame = view.bounds
    viewController.didMove(toParentViewController: self)
  }
  
  func checkCart() {
    if Cart.shared.items.count > 0 {
      cartItem.setImage(UIImage(named: "iconNewItem"), for: .normal)
    } else {
      cartItem.setImage(UIImage(named: "icnCart"), for: .normal)
    }
  }
}
