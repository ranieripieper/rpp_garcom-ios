//
//  PlaceCell.swift
//  Garcom
//
//  Created by Gilson Gil on 08/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class PlaceCell: UITableViewCell {
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var distanceLabel: UILabel!
  @IBOutlet weak var placeImageView: UIImageView!
  
  func configure(with place: Place) {
    nameLabel.text = place.name
    addressLabel.text = place.address
    distanceLabel.text = place.distance
    placeImageView.image = UIImage(named: place.imageName)
  }
}
