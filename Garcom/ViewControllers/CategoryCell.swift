//
//  CategoryCell.swift
//  Garcom
//
//  Created by Gilson Gil on 08/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class CategoryCell: UITableViewCell {
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var categoryImageView: UIImageView!
  
  func configure(with category: Category) {
    nameLabel.text = category.name
    categoryImageView.image = UIImage(named: category.imageName)
  }
}
