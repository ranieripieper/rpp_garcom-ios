//
//  HistoryCell.swift
//  Garcom
//
//  Created by Gilson Gil on 12/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class HistoryCell: UITableViewCell {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var valueLabel: UILabel!
  @IBOutlet weak var stateLabel: UILabel!
  @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
  
  fileprivate var item: HistoryItem?
  
  func configure(with item: HistoryItem, timeFormatter: DateFormatter) {
    tableView.rowHeight = 30
    self.item = item
    dateLabel.text = timeFormatter.string(from: item.date)
    valueLabel.text = item.formattedTotal
    switch item.state {
    case .delivered:
      stateLabel.backgroundColor = UIColor(red: 202 / 255, green: 214 / 255, blue: 189 / 255, alpha: 1)
      stateLabel.text = "PEDIDO ENTREGUE"
    case .refused:
      stateLabel.backgroundColor = UIColor(red: 213 / 255, green: 192 / 255, blue: 192 / 255, alpha: 1)
      stateLabel.text = "PEDIDO RECUSADO"
    }
    tableView.reloadData()
  }
}

extension HistoryCell: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    let count = item?.items.count ?? 0
    tableViewHeightConstraint.constant = max(0, CGFloat(count) * tableView.rowHeight)
    return count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let historyItem = self.item else {
      return UITableViewCell()
    }
    let item = historyItem.items[indexPath.row]
    guard let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(PaymentCell.self).components(separatedBy: ".").last!, for: indexPath) as? PaymentCell else {
      return UITableViewCell()
    }
    cell.configure(with: item)
    return cell
  }
}
