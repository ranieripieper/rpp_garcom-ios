//
//  CartSummaryFooter.swift
//  Garcom
//
//  Created by Gilson Gil on 12/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol CartSummaryFooterDelegate: class {
  func purchaseTapped()
}

final class CartSummaryFooter: UITableViewHeaderFooterView {
  @IBOutlet weak var valueLabel: UILabel!
  @IBOutlet weak var totalLabel: UILabel!
  
  weak var delegate: CartSummaryFooterDelegate?
		
  
  override func awakeFromNib() {
    super.awakeFromNib()
    valueLabel.text = Cart.shared.formattedTotal
    totalLabel.text = Cart.shared.formattedTotalWithService
    contentView.backgroundColor = UIColor(white: 243 / 255, alpha: 1)
  }
}

// MARK: - Actions
extension CartSummaryFooter {
  @IBAction func purchaseTapped(sender: UIButton) {
    delegate?.purchaseTapped()
  }
}
