//
//  PurchaseViewController.swift
//  Garcom
//
//  Created by Gilson Gil on 11/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class PurchaseViewController: UIViewController {
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!
  @IBOutlet weak var valueLabel: UILabel!
  @IBOutlet weak var counterLabel: UILabel!
  
  var product: Product?
  fileprivate var counter = 1 {
    didSet {
      counterLabel.text = String(counter)
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    nameLabel.text = product?.name
    descriptionLabel.text = product?.description
    valueLabel.text = product?.formattedValue
  }
}

// MARK: - Actions
extension PurchaseViewController {
  @IBAction func subtractTapped(sender: UIButton) {
    counter = max(0, counter - 1)
  }
  
  @IBAction func addTapped(sender: UIButton) {
    counter += 1
  }
  
  @IBAction func addToCartTapped(sender: UIButton) {
    guard let product = product else {
      return
    }
    guard counter > 0 else {
      dismiss(animated: true, completion: nil)
      return
    }
    let cartItem = CartItem(product: product, amount: counter)
    Cart.shared.add(item: cartItem)
    ((presentingViewController as? UINavigationController)?.viewControllers.last as? ProductsViewController)?.checkCart()
    dismiss(animated: true, completion: nil)
  }
}
