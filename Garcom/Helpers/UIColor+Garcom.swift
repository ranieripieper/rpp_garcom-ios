//
//  UIColor+Garcom.swift
//  Garcom
//
//  Created by Gilson Gil on 26/10/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

extension UIColor {
  static func textFieldPlaceholderColor() -> UIColor {
    return self.init(colorLiteralRed: 84 / 255, green: 105 / 255, blue: 121 / 255, alpha: 1)
  }
}
