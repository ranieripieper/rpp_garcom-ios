//
//  TextField.swift
//  Garcom
//
//  Created by Gilson Gil on 26/10/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class TextField: UITextField {
  var leftViewImage: UIImage!
  var rightViewImage: UIImage!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    layoutIfNeeded()
    attributedPlaceholder = NSAttributedString(string: placeholder!, attributes: [NSForegroundColorAttributeName: UIColor.textFieldPlaceholderColor(), NSFontAttributeName: UIFont(name: "OpenSans", size: 16)!])
    let leftView = UIImageView(image: self.leftViewImage)
    leftView.frame = CGRect(x: 0, y: 0, width: bounds.height, height: bounds.height)
    leftView.contentMode = .center
    self.leftView = leftView
    self.leftViewMode = .always
    switch tag {
    case 4:
      let datePicker = UIDatePicker()
      datePicker.datePickerMode = .date
      datePicker.calendar = Calendar.current
      datePicker.locale = Locale.current
      inputView = datePicker
    case 5:
      let pickerView = UIPickerView()
      inputView = pickerView
    default:
      break
    }
    
    guard let rightViewImage = rightViewImage else {
      return
    }
    let rightView = UIButton(type: .custom)
    rightView.frame = CGRect(x: 0, y: 0, width: bounds.height, height: bounds.height)
    rightView.setImage(rightViewImage, for: .normal)
    rightView.addTarget(self, action: #selector(togglePasswordReveal), for: .touchUpInside)
    self.rightView = rightView
    self.rightViewMode = .always
  }
  
  func togglePasswordReveal() {
    isSecureTextEntry = !isSecureTextEntry
  }
}
