//
//  LoginNavigationBar.swift
//  Garcom
//
//  Created by Gilson Gil on 26/10/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class LoginNavigationBar: UINavigationBar {
  override func awakeFromNib() {
    super.awakeFromNib()
    setBackgroundImage(UIImage(), for: .default)
    shadowImage = UIImage()
  }
}
